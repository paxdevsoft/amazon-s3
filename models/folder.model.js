require('./user.model');

const dynamoose = require("dynamoose");

var User = dynamoose.model('User');

const schema = new dynamoose.Schema({
    id: {
        hashKey: true,
        type: String,
    },
    name_folder: {
        type: String,
        required: true
    },
    user: User,
    active_folder: {
        type: Boolean,
        default: true
    }
}, {
    saveUnknown: false,
    timestamps: true
});

module.exports = dynamoose.model("Folder", schema);
