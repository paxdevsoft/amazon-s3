require('./folder.model');

const dynamoose = require("dynamoose");

var Folder = dynamoose.model('Folder');

const schema = new dynamoose.Schema({
    id: {
        hashKey: true,
        type: String,
    },
    name_file: {
        type: String,
        required: true
    },
    folder: Folder,
    location_file: {
        type: String,
        required: true
    },
    url_file: {
        type: String,
        required: true
    },
    active_file: {
        type: Boolean,
        default: true
    }
}, {
    saveUnknown: false,
    timestamps: true
});

module.exports = dynamoose.model("File", schema);
