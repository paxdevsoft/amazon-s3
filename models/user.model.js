const dynamoose = require("dynamoose");

const schema = new dynamoose.Schema({
    id: {
        type: String,
        required: true
    },
    name_user: {
        type: String,
        required: true
    },
    last_name_user: {
        type: String,
        required: true
    },
    email_user: {
        type: String,
        required: true,
        hashKey: true
    },
    password_user: {
        type: String,
        required: true
    },
    active_user: {
        type: Boolean,
        default: true
    }
}, {
    saveUnknown: false,
    timestamps: true
});

module.exports = dynamoose.model("User", schema);
