var express = require('express');
var passport = require('passport');
var router = express.Router();

const controller = require('../controllers/aws.controller');

/* GET home page. */
router.get('/test/:name/:last', (req, res, next) => {
  var limpiar = function(str){
    str = str.replace(/á/g, "a");
    str = str.replace(/é/g, "e");
    str = str.replace(/í/g, "i");
    str = str.replace(/ó/g, "o");
    str = str.replace(/ú/g, "u");
    str = str.replace(/\s/g, "");
    return str;
  }

  var bucketParams = {
    Bucket : limpiar(req.params.name).toLowerCase() + "-" + limpiar(req.params.last).toLowerCase()
  };
  console.log(bucketParams);
  res.send(bucketParams);
});
/* GET home page. */
router.get('/', (req, res, next) => {
    req.flash;
    res.render('login', {title: 'Login', message: req.flash('message')});
});

/* GET home page. */
router.get('/home', controller.list_files_in_Buckets_view);
router.get('/home/:folder', controller.list_files_in_Folder_view);

/* Authentication */
router.post('/save', passport.authenticate('local-signup', {session: false, successRedirect: '/', failureRedirect: '/'}));
router.get('/verify', passport.authenticate('localapikey', {successRedirect: '/registro', failureRedirect: '/register' }));
router.post('/auth', passport.authenticate('local-signin',  { successRedirect: '/home', failureRedirect: '/' }));
router.get('/logout', function(req, res, next){
    req.logout();
    req.session.destroy(function (err) {
        res.redirect('/');
    });
});

module.exports = router;
