var express = require('express');
var router = express.Router();

const controller = require('../controllers/aws.controller');
/**  CONTROLLERS */
router.get('/', controller.get_Buckets);
router.post('/create/folder', controller.create_folder_In_Buckets);
router.post('/create/bucket', controller.create_Bucket);
router.get('/delete/bucket/:name', controller.delete_Bucket);
router.get('/delete/file/:name', controller.delete_file_Bucket);
router.get('/delete/folder/:name', controller.delete_folder_Bucket);
router.post('/create/file', controller.create_File_In_A_Bucket);
router.get('/get/files/:name', controller.get_Files_In_A_Bucket);
router.get('/buckets', controller.list_Buckets);
router.get('/download/:name', controller.dowload_File_In_A_Bucket);

/** VIEWS */

module.exports = router;
