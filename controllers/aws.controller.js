'use strict'
var fs = require('fs');
var path = require('path');
var AWS = require("aws-sdk");

const User = require('../models/user.model');
const File = require('../models/file.model');

AWS.config.update({
  accessKeyId: aws_access_key_id,
  secretAccessKey: aws_secret_access_key,
  region: 'sa-east-1'
});

// Create S3 service object
const s3 = new AWS.S3();

/* Obtener la lista de Buckets */
exports.get_Buckets = function (req, res, next) {
  s3.listBuckets(function(err, data) {
    if (err) {
      console.log("Error", err);
      next(err);
    } else {
      console.log("Success", data);
      res.send(data.Buckets);
    }
  });
};

/* Crear Bucket */
exports.create_Bucket = function (req, res, next) {
  // Create the parameters for calling createBucket
  let name_bucket = req.body.name_file.toLowerCase();
  var res_test = /^[A-Za-z0-9\-]+$/g.test(name_bucket);
  if (res_test) {
    var bucketParams = {
      Bucket : name_bucket
    };

    // call S3 to create the bucket
    s3.createBucket(bucketParams, function(err, data) {
      if (err) {
        console.error("Error", err);
        next(err);
      } else {
        console.log("Success", data);
        res.redirect("/buckets");
      }
    });
  }else{
    res.end("El nombre contiene caracteres especiales");
  }
};

/* Eliminar Bucket */
exports.delete_Bucket = function (req, res, next) {
  // Create params for S3.deleteBucket
  var bucketParams = {
    Bucket : req.params.name
  };

  // Call S3 to delete the bucket
  s3.deleteBucket(bucketParams, function(err, data) {
    if (err) {
      console.log("Error", err);
      next(err);
    } else {
      console.log("Success", data);
      res.send(data);
    }
  });
};

/* Eliminar Archivos */
exports.delete_file_Bucket = function (req, res, next) {
  req.flash;
  if(!req.isAuthenticated()){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
    res.redirect('/');
  }else{
    // Create the parameters for calling listObjects
    var bucketParams = {
      Bucket : req.user.name_user.toLowerCase() + "-" + req.user.last_name_user.toLowerCase() + "-" + req.user.id.toLowerCase(),
      Key: req.params.name
    };

    // Call S3 to obtain a list of the objects in the bucket
    s3.deleteObject(bucketParams, function(err, data) {
      if (err) {
        console.log("Error", err);
        next(err);
      } else {
        console.log("Success", data);
        res.send(data);
      }
    });
  }
};

async function emptyS3Directory(bucket, dir) {
  const listParams = {
    Bucket: bucket,
    Prefix: dir
  };

  const listedObjects = await s3.listObjectsV2(listParams).promise();

  if (listedObjects.Contents.length === 0) return;

  const deleteParams = {
    Bucket: bucket,
    Delete: { Objects: [] }
  };

  listedObjects.Contents.forEach(({ Key }) => {
    deleteParams.Delete.Objects.push({ Key });
  });

  await s3.deleteObjects(deleteParams).promise();

  if (listedObjects.Contents.IsTruncated) await emptyS3Directory(bucket, dir);
}

/* Eliminar Archivos */
exports.delete_folder_Bucket = function (req, res, next) {
  req.flash;
  if(!req.isAuthenticated()){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
    res.redirect('/');
  }else{
    let f = (req.params.name.slice(-1) == "/")? req.params.name : req.params.name + "/";
    var bucket = req.user.name_user.toLowerCase() + "-" + req.user.last_name_user.toLowerCase() + "-" + req.user.id.toLowerCase();
    var recursivo = emptyS3Directory(bucket, f);
    setTimeout(function(){
      req.flash('message', {icon: "success", msg: 'Carpeta eliminada'});
      res.redirect('/home');
    }, 1000);
  }
};

/* Crear un archivo dentro de un Bucket */
exports.create_File_In_A_Bucket = function (req, res, next) {
  req.flash;
  if(!req.isAuthenticated()){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
    res.redirect('/');
  }else{
    var file = req.files.file;
    let f = (req.body.parent)? req.body.parent + file.name : file.name;
    // call S3 to retrieve upload file to specified bucket
    var uploadParams = {
      Bucket: req.user.name_user.toLowerCase() + "-" + req.user.last_name_user.toLowerCase() + "-" + req.user.id.toLowerCase(),
      Key: f,
      Body: file.data,
      ACL: 'public-read'
    };
    console.log(uploadParams);
    // call S3 to retrieve upload file to specified bucket
    s3.upload (uploadParams, function (err, data) {
      if (err) {
        console.log("Error upload file: ", err);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error'});
        res.redirect('/home');
      } if (data) {
        console.log("Upload Success", data);
        req.flash('message', {icon: "success", msg: 'Archivo Cargado con Éxito'});
        res.redirect('/home');
      }
    });
  }
};

/* obtener archivos dentro de un Bucket */
exports.get_Files_In_A_Bucket = function (req, res, next) {
  // Create the parameters for calling listObjects
  var bucketParams = {
    Bucket : req.params.name,
  };

  // Call S3 to obtain a list of the objects in the bucket
  s3.listObjects(bucketParams, function(err, data) {
    if (err) {
      console.log("Error", err);
      next(err);
    } else {
      console.log("Success", data);
      res.send(data);
    }
  });
};

/* obtener archivos dentro de un Bucket */
exports.dowload_File_In_A_Bucket = function (req, res, next) {
  req.flash;
  if(!req.isAuthenticated()){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
    res.redirect('/');
  }else{
    let params = {
      Bucket: req.user.name_user.toLowerCase() + "-" + req.user.last_name_user.toLowerCase() + "-" + req.user.id.toLowerCase(),
      Key: req.params.name
    };
    s3.getObject(params, (err, data) => {
      if (err) {
        console.error(err);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error'});
        res.redirect('/home');
      }else{
        let filename = req.params.name.substring(req.params.name.lastIndexOf("/")+1);
        //let blob = new Blob([data.Body], {type: data.ContentType});
        fs.writeFile(filename, data.Body, 'binary', (error)=>{
          if (error) {
            console.error(error);
          }else{
            let absPath = path.join(__dirname.replace("/controllers", ""), '/', filename);
            res.download(absPath, (err) => {
              if (err) {
                console.log(err);
              }else{
                fs.unlink(filename, (err) => {
                  if (err) {
                    console.log(err);
                  }else{
                    console.log('FILE [' + filename + '] REMOVED!');
                  }
                });
              }
            });
          }
        });
      }
    });
  }
};

/* crear carpeta en Bucket */
exports.create_folder_In_Buckets = function (req, res, next) {
  req.flash;
  if(!req.isAuthenticated()){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
    res.redirect('/');
  }else{
    let f = (req.body.parent)? req.body.parent + req.body.name + '/' : req.body.name + '/';
    var params = {
      Bucket: req.user.name_user.toLowerCase() + "-" + req.user.last_name_user.toLowerCase() + "-" + req.user.id.toLowerCase(),
      Key: f,
      ACL: 'public-read',
      Body:'body does not matter'
    };

    s3.upload(params, function (err, data) {
      if (err) {
        console.log("Error creating the folder: ", err);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error'});
        res.redirect('/home');
      } else {
        console.log("Successfully created a folder on S3");
        req.flash('message', {icon: "success", msg: 'Carpeta Creada con Éxito'});
        res.redirect('/home');
      }
    });
  }
};

/*VIEWS*/

/* Obtener la lista de Buckets */
exports.list_Buckets = function (req, res, next) {
  req.flash;
  if(!req.isAuthenticated()){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
    res.redirect('/');
  }else{
    s3.listBuckets(function(err, data) {
      if (err) {
        console.log("Error", err);
        next(err);
      } else {
        console.log("Success", data);
        res.render('buckets', {
          buckets: data.Buckets,
          title: 'Listado de Buckets',
          message: req.flash('message')
        });
      }
    });
  }
};

/* Obtener la lista de Buckets */
exports.list_files_in_Buckets_view = function (req, res, next) {
  req.flash;
  if(!req.isAuthenticated()){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
    res.redirect('/');
  }else{
    // Create the parameters for calling listObjects
    var bucketParams = {
      Bucket : req.user.name_user.toLowerCase() + "-" + req.user.last_name_user.toLowerCase() + "-" + req.user.id.toLowerCase()
    };

    // Call S3 to obtain a list of the objects in the bucket
    s3.listObjects(bucketParams, function(err, data) {
      if (err) {
        console.log("Error", err);
        req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
        res.redirect('/');
      } else {
        console.log("Success", data);
        res.render('home', {
          buckets: data.Contents,
          folder: null,
          title: 'Listado de Archivos',
          message: req.flash('message')
        });
      }
    });
  }
};

/* Obtener la lista de Buckets */
exports.list_files_in_Folder_view = function (req, res, next) {
  req.flash;
  if(!req.isAuthenticated()){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
    res.redirect('/');
  }else{
    // Create the parameters for calling listObjects
    let f = (req.params.folder.slice(-1) == "/")? req.params.folder : req.params.folder + "/";
    var bucketParams = {
      Bucket : req.user.name_user.toLowerCase() + "-" + req.user.last_name_user.toLowerCase() + "-" + req.user.id.toLowerCase(),
      Delimiter: "/",
      Prefix: f
    };

    // Call S3 to obtain a list of the objects in the bucket
    s3.listObjects(bucketParams, function(err, data) {
      if (err) {
        console.log("Error", err);
        req.flash('message', {icon: "warning", msg: 'No puedes acceder sin iniciar sesión'});
        res.redirect('/');
      } else {
        console.log("Success", data);
        res.render('home', {
          buckets: data.Contents,
          folder: f,
          title: 'Listado de Archivos',
          message: req.flash('message')
        });
      }
    });
  }
};
