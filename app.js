var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var passport = require('passport');
var session = require('express-session');
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var fs = require('fs');
const fileUpload = require('express-fileupload');
const dynamoose = require("dynamoose");


global.aws_access_key_id = "AKIAYTN6TNIOUAS6WR4K";
global.aws_secret_access_key = "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";

dynamoose.aws.sdk.config.update({
  "accessKeyId": aws_access_key_id,
  "secretAccessKey": aws_secret_access_key,
  "region": "us-east-1"
});

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bucketRouter = require('./routes/bucket.route');

var app = express();

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false}));

require('./config/passport/passport.js')(passport);

//For Passport
app.use(session({secret: 'qwertyuioopasdfghjklñzxcvbnm7410852963', resave: true, saveUninitialized: true}));//session secret
app.use(passport.initialize());
app.use(passport.session());// Persistent login session

app.use(fileUpload());

//conect-flash
app.use(flash());


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bucket', bucketRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
