function getDateString(date, format){
    var months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    var date = new Date(date);

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var add = (hour == "19" && min == "00" && sec == "00")? 1 : 0;
    var day = date.getDate() + add;
    day = (day < 10 ? "0" : "") + day;
    var out = "";
    out = format.replace(/DD/g, day);
    out = out.replace(/MM/g, month);
    out = out.replace(/S_MONTH/g, months[month-1]);
    out = out.replace(/YYYY/g, year);
    out = out.replace(/HH/g, hour);
    out = out.replace(/MIN/g, min);
    return out;
}
