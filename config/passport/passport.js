var bCrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer');
const fileUpload = require('express-fileupload');
const path = require('path');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
var AWS = require("aws-sdk");

AWS.config.update({
  accessKeyId: aws_access_key_id,
  secretAccessKey: aws_secret_access_key,
  region: 'sa-east-1'
});

// Create S3 service object
const s3 = new AWS.S3();

const User = require('../../models/user.model');

module.exports = function (passport) {

  var LocalStrategy = require('passport-local').Strategy;
  var LocalAPIKeyStrategy = require("passport-localapikey-update").Strategy;

  passport.serializeUser(function (account, done) {
    done(null, account);
  });

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    if(id){
      done(null, id);
    } else {
      done(id.errors, null);
    }
  });
  //registro de usuarios por passport
  passport.use('local-signup', new LocalStrategy(
    {
      usernameField: 'email_user',//lo que esta como name en el input del registro
      passwordField: 'password_user',//lo que esta como name en el input del registro
      passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function (req, email, password, done) {
      var generateHash = function (password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
      };
      if(!email && !password && !req.body.name_user && !req.body.last_name_user){
        console.error("Campos faltantes");
        req.flash('message', {icon: "warning", msg: 'Debe ingresar todos los campos.'});
        return done(null, false, {
          message: 'Debe Ingresar Todos los datos'
        });
      }else{
        var emailRegEx = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        if(!emailRegEx.test(email)){
          console.error("Email Incorrecto");
          req.flash('message', {icon: "error", msg: 'El correo ingresado es incorrecto.'});
          return done(null, false, {
            message: 'El Correo Ingresado es Incorrecto'
          });
        }else{
          req.body.id = uuidv4();
          req.body.password_user = generateHash(password);
          User.scan("email_user").eq(email).exec((error_get, user_get)=>{
            if (error_get) {
              console.error("Error get user passport sing up: ", error_get);
              req.flash('message', {icon: "error", msg: 'El correo ingresado es incorrecto.'});
              return done(null, false, {
                message: 'El Correo Ingresado es Incorrecto'
              });
            }else{
              if (user_get && user_get.length > 0) {
                user_get = user_get[0].toJSON();
              }else{
                console.error(user_get);
                user_get = undefined;
              }
              if (user_get) {
                console.error("Correo repetido");
                req.flash('message', {icon: "error", msg: 'El correo ingresado ya existe.'});
                return done(null, false, {
                  message: 'El correo ingresado ya existe'
                });
              }else{
                var new_user = new User(req.body);
                new_user.save((error_save)=>{
                  if (error_save) {
                    console.error("Error save user passport sing up: ", error_save);
                    req.flash('message', {icon: "error", msg: 'Algo salió mal al tratar de registrar su cuenta.'});
                    return done(null, false, {
                      message: 'Algo salió mal al tratar de registrar su cuenta.'
                    });
                  }else{
                    var limpiar = function(str){
                      str = str.replace(/á/g, "a");
                      str = str.replace(/é/g, "e");
                      str = str.replace(/í/g, "i");
                      str = str.replace(/ó/g, "o");
                      str = str.replace(/ú/g, "u");
                      str = str.replace(/\s/g, "");
                      return str;
                    }

                    var bucketParams = {
                      Bucket : limpiar(new_user.name_user).toLowerCase() + "-" + limpiar(new_user.last_name_user).toLowerCase() + "-" + new_user.id.toLowerCase()
                    };
                    console.log(bucketParams);
                    // call S3 to create the bucket
                    s3.createBucket(bucketParams, function(err, data) {
                      if (err) {
                        console.error("Error al crear el bucket: ", err);
                        req.flash('message', {icon: "error", msg: 'Algo salió mal al tratar de crear su Bucket.'});
                        return done(null, false, {
                          message: 'Algo salió mal al tratar de crear su Bucket.'
                        });
                      } else {
                        console.log("Success create bucket: ", data);
                        req.flash('message', {icon: "success", msg: 'Gracias por crear su cuenta.'});
                        return done(null, new_user);
                      }
                    });
                  }
                });
              }
            }
          });
        }
      }
    }
  ));
  //inicio de sesion
  passport.use('local-signin', new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'pass',
      passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function (req, email, password, done) {
      var isValidPassword = function (userpass, password) {
        return bCrypt.compareSync(password, userpass);
      }
      if(!email && !password){
        console.error("Datos faltantes");
        req.flash('message', {icon: "warning", msg: 'Debe ingresar todos los datos.'});
        return done(null, false, {
          message: 'Debe Ingresar Todos los datos'
        });
      }else{
        var emailRegEx = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        if(!emailRegEx.test(email)){
          console.error("Correo incorrecto");
          req.flash('message', {icon: "error", msg: 'El correo Ingresado es incorrecto.'});
          return done(null, false, {
            message: 'El Correo Ingresado es Incorrecto'
          });
        }else{
          User.scan("email_user").eq(email).exec((err, registry) => {
            if (err){
              console.error("Error en get login: ", err);
              req.flash('message', {icon: "error", msg: 'Ocurrió un error.'});
              return done(null, false, {message: 'Ocurrió un error.'});
            }else{
              if (registry && registry.length > 0) {
                registry = registry[0].toJSON();
              }else{
                console.error(registry);
                registry = undefined;
              }
              if (!registry) {
                console.error("no correo");
                req.flash('message', {icon: "error", msg: 'El correo no existe.'});
                return done(null, false, {message: 'Correo no existe'});
              }else{
                if(!registry.active_user){
                  console.error("no activo");
                  req.flash('message', {icon: "error", msg: 'Su cuenta esta desactivada.'});
                  return done(null, false, {message: 'Su cuenta esta Desactivada.'});
                }else{
                  if (!isValidPassword(registry.password_user, password)) {
                    console.log("clave incorrecta");
                    req.flash('message', {icon: "error", msg: 'Clave incorrecta.'});
                    return done(null, false, {message: 'Clave incorrecta.'});
                  }else{
                    req.flash('message', {icon: "success", msg: 'Bienvenido ' + registry.name_user + " " + registry.last_name_user});
                    return done(null, registry);
                  }
                }
              }
            }
          });
        }
      }
    }
  ));
  //verificar correo
  passport.use(new LocalAPIKeyStrategy(
    function(apikey, done) {
      var registry = User.get(apikey);
      if (!registry) {
        return done(null, false, {mensaje: 'No se reconoce la cuenta'});
      }else{
        registry.active_user = true;
        registry.save((err, updatedRegistry) => {
          if (err) {
            console.log("Error de Activación: ", err);
            req.flash('message', {icon: "error", msg: 'Error de Activación.'});
            return done(null, false, {message: 'Error de Activación.'});
          }else{
            console.log("cuenta activada");
            req.flash('message', {icon: "success", msg: "Su cuenta esta activada."});
            return done(null, registry);
          }
        });
      }
    }
  ));
}
